program png2bmp;

uses
  Vcl.Forms,
  v.main in 'v.main.pas' {vMain},
  Vcl.Themes,
  Vcl.Styles;

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  TStyleManager.TrySetStyle('Windows10');
  Application.CreateForm(TvMain, vMain);
  Application.Run;
end.
