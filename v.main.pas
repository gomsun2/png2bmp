unit v.main;

interface

uses
  mvw.vForm, Spring.Collections,

  Vcl.Imaging.pngimage,

  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, System.ImageList, Vcl.ImgList, PngImageList, Vcl.StdCtrls, Vcl.ExtCtrls,
  Vcl.ComCtrls, RzPanel, RzSplit, RzStatus, RzButton, RzRadChk;

type
  TvMain = class(TvForm)
    RzStatusBar1: TRzStatusBar;
    RzVersionInfo1: TRzVersionInfo;
    RzVersionInfoStatus1: TRzVersionInfoStatus;
    PageControl: TPageControl;
    TabDrag: TTabSheet;
    TabConvert: TTabSheet;
    RzSplitter1: TRzSplitter;
    Label1: TLabel;
    Panel1: TPanel;
    ButtonExe: TButton;
    ProgressBar: TProgressBar;
    ListBoxPng: TListBox;
    ListBoxBmp: TListBox;
    LabelCnt: TLabel;
    RadioDeleteFile: TRzRadioButton;
    RadioButton2: TRzRadioButton;
    procedure FormCreate(Sender: TObject);
    procedure ButtonExeClick(Sender: TObject);
  private
    FFiles: IList<string>;
    FDeleteWhenDuplicate: Boolean;
  protected
    procedure DoDropFile(const ACnt: Integer; const AFiles: TStringList); override;
  public
  end;

var
  vMain: TvMain;

implementation

{$R *.dfm}

uses
  System.Threading, System.IOUtils, Winapi.CommCtrl, Vcl.Consts, Winapi.ShlObj
  ;

type
  TPathHelper = record helper for TPath
    class function MakeUniqueFileName(const AFileName: string): string; static;
  end;

class function TPathHelper.MakeUniqueFileName(
  const AFileName: string): string;
var
  LDst: array[0..MAX_PATH-1] of Char;
  LPath: string;
  LFile: string;
begin
  LPath := TDirectory.GetParent(AFileName);
  LFile := TPath.GetFileName(AFileName);
  Result := TPath.Combine(LPath, LFile);

  if TFile.Exists(Result) then
    if PathMakeUniqueName(LDst, Length(LDst), PChar(LFile), nil, PChar(LPath)) then
      Result := LDst;
end;

{$REGION 'PNG to BMP'}
  {
    PNG to BMP
      see - https://stackoverflow.com/a/45865442/1174572
        , by Sylvio Ruiz Neto
  }
  procedure ImageList2Alpha(const ImageList: TImageList);
  const
    Mask: array[Boolean] of Longint = (0, ILC_MASK);
  var
    TempList: TImageList;
  begin
    if Assigned(ImageList) then
    begin
      TempList := TImageList.Create(nil);
      try
        TempList.Assign(ImageList);
        with ImageList do
        begin
          Handle := ImageList_Create(Width, Height, ILC_COLOR32 or Mask[Masked], 0, AllocBy);
          if not HandleAllocated then
            raise EInvalidOperation.Create(SInvalidImageList);
        end;
        Imagelist.AddImages(TempList);
      finally
        FreeAndNil(TempList);
      end;
    end;
  end;

  procedure LoadPngToBmp(var ADest: TBitmap; AFilename: TFilename);
  type
    TRGB32 = packed record
      B, G, R, A : Byte;
    end;
    PRGBArray32 = ^TRGBArray32;
    TRGBArray32 = array[0..0] of TRGB32;
  type
    TRG24 = packed record
      rgbtBlue, rgbtGreen, rgbtRed : Byte;
    end;
    PRGBArray24 = ^TPRGBArray24;
    TPRGBArray24 = array[0..0] of TRG24;
  type
    TByteArray = Array[Word] of Byte;
    PByteArray = ^TByteArray;
    TPByteArray = array[0..0] of TByteArray;
  var
    LBmp : TBitmap;
    LPng: TPngImage;
    x, y: Integer;
    LBmpRow: PRGBArray32;
    LPngRow : PRGBArray24;
    LAlphaRow: PByteArray;
  begin
    LBmp := TBitmap.Create;
    LPng := TPNGImage.Create;
    try
      if TFile.Exists(AFilename) then
      begin
        LPng.LoadFromFile(AFilename);
        LBmp.PixelFormat := pf32bit;
        LBmp.Height := LPng.Height;
        LBmp.Width := LPng.Width;
        if LPng.TransparencyMode = ptmPartial then
          for y := 0 to LBmp.Height-1 do
          begin
            LBmpRow := PRGBArray32(LBmp.ScanLine[y]);
            LPngRow := PRGBArray24(LPng.ScanLine[y]);
            LAlphaRow := PByteArray(LPng.AlphaScanline[y]);
            for x := 0 to LBmp.Width - 1 do
            begin
              with LBmpRow[x] do
              begin
                with LPngRow[x] do
                begin
                  R := rgbtRed; G := rgbtGreen; B := rgbtBlue;
                end;
                A := Byte(LAlphaRow[x]);
              end;
            end;
          end
        else
          for y := 0 to LBmp.Height-1 do
          begin
            LBmpRow := PRGBArray32(LBmp.ScanLine[y]);
            LPngRow := PRGBArray24(LPng.ScanLine[y]);
            for x := 0 to LBmp.Width - 1 do
            begin
              with LBmpRow[x] do
              begin
                with LPngRow[x] do
                begin
                  R := rgbtRed; G := rgbtGreen; B := rgbtBlue;
                end;
                A := 255;
              end;
            end;
          end;

        ADest.Assign(LBmp);
      end;
    finally
      FreeAndNil(LBmp);
      FreeAndNil(LPng);
    end;
  end;

{$ENDREGION}

procedure TvMain.ButtonExeClick(Sender: TObject);
begin
  ButtonExe.Enabled := False;
  FDeleteWhenDuplicate := RadioDeleteFile.Checked;
  TTask.Run(procedure
    begin
      TParallel.&For(0, FFiles.Count -1,
        procedure(i: Integer)
        var
          LBmp: TBitmap;
          LFileName: string;
        begin
          LBmp := TBitmap.Create;
          try
            LoadPngToBmp(LBmp, FFiles[i]);
            LFileName := TPath.ChangeExtension(FFiles[i], '.bmp');
            if TFile.Exists(LFileName) then
              if FDeleteWhenDuplicate then
                TFile.Delete(LFileName)
              else
                LFileName := TPath.MakeUniqueFileName(LFileName);
            LBmp.SaveToFile(LFileName);
            TThread.Synchronize(TThread.Current, procedure
              begin
                ListBoxBmp.Items.BeginUpdate;
                ListBoxBmp.Items[i] := TPath.GetFileName(LFileName);
                ListBoxBmp.Items.EndUpdate;
                ProgressBar.StepIt;
                LabelCnt.Caption := Format('%d/%d', [ProgressBar.Position, ProgressBar.Max]);
                ButtonExe.Enabled := i = FFiles.Count -1;
              end);
          finally
            FreeAndNil(LBmp);
          end;
        end);
    end);
end;

{ TvMain }

procedure TvMain.DoDropFile(const ACnt: Integer; const AFiles: TStringList);
var
  i: Integer;
begin
  if AFiles.Count = 0 then
    Exit;

  ListBoxBmp.Items.BeginUpdate;
  ListBoxPng.Items.BeginUpdate;
  try
    ListBoxPng.Clear;
    ListBoxBmp.Clear;
    for i := 0 to AFiles.Count -1 do
    begin
      ListBoxPng.Items.Add(TPath.GetFileName(AFiles[i]));
      ListBoxBmp.Items.Add('');
    end;
  finally
    ListBoxPng.Items.EndUpdate;
    ListBoxBmp.Items.EndUpdate;
  end;
  ProgressBar.Position := 0;
  ProgressBar.Max := AFiles.Count;
  PageControl.ActivePageIndex := 1;
  FFiles.Clear;
  FFiles.AddRange(AFiles.ToStringArray);
end;

procedure TvMain.FormCreate(Sender: TObject);
var
  i: Integer;
begin
  for i := 0 to PageControl.PageCount -1 do
    PageControl.Pages[i].TabVisible := False;
  PageControl.ActivePageIndex := 0;

  FFiles := TCollections.CreateList<string>;

  EnableDropdownFiles := True;
  DropDownFileExt := '.png';
end;

end.
