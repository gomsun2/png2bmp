- Download: [1.0.0.4](./release/png2bmp.exe)

# png2bmp

The pnp2bmp is a simple tool for convert png to bmp file.

- multi-core(parellel) support
- reflects the transparent color from a png. (32bit bitmap support)

## how to use

1. drag the png files to the png2bmp
2. click the Execute button.

# png2bmp

png2bmp는 복수개의 png파일을 bmp파일로 변환하는 간단한 도구 입니다.

- 멀티 코어(패럴럴) 지원
- png의 투명색이 반영됩니다.(32bit 비트맵 지원)

## 사용방법

1. png파일들을 드래그 후 png2bmp에 드롭합니다.
2. Execute 버튼을 클릭 합니다.

## Screenshot

![screen shot](png2bmp.png)